import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch,Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./components/Login";
import Register from "./components/Register";
import Selltickets from "./components/Selltickets";
import Createticket from "./components/Createticket";
import Update from "./components/Update";
import Order from "./components/Order";
import Myorder from "./components/Myorder";
import Createpayment from "./components/Createpayment";
import Success from "./components/Success";

import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";
import { history } from './helpers/history';


import EventBus from "./common/EventBus";
import TicketList from "./components/TicketList";


class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); 
    });
  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user
        
      });
    }

    EventBus.on("logout", () => {
      this.logOut();
    });
  }

  componentWillUnmount() {
    EventBus.remove("logout");
  }

  logOut() {
    this.props.dispatch(logout());
    this.setState({
      
      currentUser: undefined,
    });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <Router history={history}>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <Link  className="navbar-brand">
              Ticketing
            </Link>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/ticketlist"} className="nav-link">
                  Home
                </Link>
              </li>

             
             
            </div>

            {currentUser ? (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
              <Link to={"/myorders"} className="nav-link">
               My Orders
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/selltickets"} className="nav-link">
              Sell Tickets
              </Link>
            </li>

                <li className="nav-item">
                  <a href="/login" className="nav-link" onClick={this.logOut}>
                    LogOut
                  </a>
                </li>
              </div>
              
              
            ) : (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link">
                    Sign In
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </li>
              </div>
            )}
          </nav>

          <div className="container mt-3">
         <div className="auth-wrapper">
          <div className="auth-inner">

            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/ticketlist" component={TicketList}/>
              <Route exact path="/selltickets" component={Selltickets}/>
              <Route exact path="/create" component={Createticket}/>
              <Route exact path="/update" component={Update}/>
              <Route exact path="/order" component={Order}/>
              <Route exact path="/myorders" component={Myorder}/>
              <Route exact path="/payment" component={Createpayment}/>
              <Route exact path="/success" component={Success}/>
            </Switch>
          </div>
         </div>
         </div>
       </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);
