import {
    CREATE_TICKET,
    RETRIEVE_TICKETS,
    UPDATE_TICKET,
    NEW_ORDER,
    NEW_PAYMENT,
} from "./types";

import UserService from "../services/UserService";


export const createTicket=(title,price,quantity)=>async(dispatch)=>{
    try{
        const res=await UserService.create({title,price,quantity});

        dispatch({
            type:CREATE_TICKET,
            payload:res.data,
        });

        return Promise.resolve(res.data);
    } catch (err) {
        return Promise.reject(err);
      }
};

export const retrieveTickets= () => async (dispatch) => {
    try {
      const res = await UserService.getAll();
  
      dispatch({
        type: RETRIEVE_TICKETS,
        payload: res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  export const updateTicket = (id, data) => async (dispatch) => {
    try {
      const res = await UserService.update(id, data);
  
      dispatch({
        type: UPDATE_TICKET,
        payload: data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const findTicketById=(id)=>async(dispatch)=>{
    try{
      const res=await UserService.findById(id);

      dispatch({
        type:RETRIEVE_TICKETS,
        payload:res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };

  export const newOrder=()=>async(dispatch)=>{
    try{
        const res=await UserService.createOrder();

        dispatch({
            type:NEW_ORDER,
            payload:res.data,
        });

        return Promise.resolve(res.data);
    } catch (err) {
        return Promise.reject(err);
      }
};

export const newPayment=(cardno,expirydate)=>async(dispatch)=>{
  try{
      const res=await UserService.createPayment({cardno,expirydate});

      dispatch({
          type:NEW_PAYMENT,
          payload:res.data,
      });

      return Promise.resolve(res.data);
  } catch (err) {
      return Promise.reject(err);
    }
};
 


