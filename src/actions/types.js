export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT = "LOGOUT";

export const SET_MESSAGE = "SET_MESSAGE";
export const CLEAR_MESSAGE = "CLEAR_MESSAGE";


export const CREATE_TICKET="CREATE_TICKET";
export const RETRIEVE_TICKETS="RETRIEVE_TICKETS";
export const UPDATE_TICKET="UPDATE_TICKET";
export const NEW_ORDER="NEW_ORDER";
export const NEW_PAYMENT="NEW_PAYMENT";