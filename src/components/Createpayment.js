import React,{useState} from "react";
import { useDispatch } from "react-redux";
import { newPayment } from "../actions/tickets";
import { useHistory } from "react-router-dom";


const Createpayment = () =>{
    
    const initialPaymentState = {
        id:null,
        cardno:"",
        expirydate:""
    };

    let history = useHistory()
    const [payment,setPayment]=useState(initialPaymentState);
    const dispatch = useDispatch();

    const handleInputChange = event =>{
        const { name, value} = event.target;
        setPayment({...payment,[name]:value});
    };

    const savePayment = () =>{
        const {cardno,expirydate} = payment;

        dispatch(newPayment(cardno,expirydate))
        .then(data => {
            setPayment({
                id:data.id,
                cardno:data.cardno,
                expirydate:data.expirydate
                
            });
            console.log(data);

        })
        .catch(e=>{
            console.log(e);
        });
        history.push("/success");
       
    };

    

    return (
    <div>
      <br />
   
        <form  className="d-grid gap-2" style={{margin:"0rem"}} >
        <section>
        <div className='card card-body mt-3'>
           
          <label className="mb-3" >
           <input type="text" className="form-control" placeholder="Enter Card No"  id='cardno' onChange={handleInputChange} name="cardno" />
          </label>
          
          <label className="mb-3" >
              <input type="text" className="form-control" placeholder="Enter Expiry Date"  id='expirydate' onChange={handleInputChange} name="expirydate" />
          </label>
           <div className='d-flex justify-content-around'>
           <button onClick={savePayment} className="btn btn-dark">Submit</button>
           
           </div>
           </div>
           
           </section>
        </form>
     </div>
   
  );

}

export default Createpayment;