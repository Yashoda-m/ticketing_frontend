import React,{useState} from "react";
import { useDispatch } from "react-redux";
import { createTicket } from "../actions/tickets";
import { useHistory } from "react-router-dom";


const AddTicket = () =>{

    let history=useHistory()
    
    const initialTicketState = {
        id:null,
        title:"",
        price:"",
        quantity:""
    };

    const [ticket,setTicket]=useState(initialTicketState);
    const dispatch = useDispatch();

    const handleInputChange = event =>{
        const { name, value} = event.target;
        setTicket({...ticket,[name]:value});
    };

    const saveTicket = () =>{
        const {title,price,quantity} = ticket;

        dispatch(createTicket(title,price,quantity))
        .then(data => {
            setTicket({
                id:data.id,
                title:data.title,
                price:data.price,
                quantity:data.quantity
            });
            console.log(data);
        })
        .catch(e=>{
            console.log(e);
        });
        history.push("/selltickets");
    };

    const newTicket =()=>{
        setTicket(initialTicketState);
    };

    return (
    <div>
      <br />
   
        <form  className="d-grid gap-2" style={{margin:"0rem"}} >
        <section>
        <div className='card card-body mt-3'>
           
          <label className="mb-3" >
           <input type="text" className="form-control" placeholder="Enter Title"  id='title' onChange={handleInputChange} name="title" />
          </label>
          
          <label className="mb-3" >
              <input type="text" className="form-control" placeholder="Enter Price"  id='price' onChange={handleInputChange} name="price" />
          </label>
            
             
         
              <label className="mb-3" >
              <input type="text" className="form-control" id='quantity' placeholder="Enter Quantity"  onChange={handleInputChange} name="quantity"/>
              </label>  
           
           <div className='d-flex justify-content-around'>
           <button onClick={saveTicket} className="btn btn-dark">Add Ticket</button>
           
           </div>
           </div>
           
           </section>
        </form>
     </div>
   
  );

}

export default AddTicket;