import React,{useState} from "react";
import {Link} from "react-router-dom";
import { useDispatch } from "react-redux";
import { newOrder } from "../actions/tickets";

const Order = () =>{

    const initialOrderState = {
        id:null,
        
    };

    const [order,setOrder]=useState(initialOrderState);
    const dispatch = useDispatch();

    const saveOrder = () =>{
        const { } = order;

        dispatch(newOrder( ))
        .then(data => {
            setOrder({
                id:data.id
            });
            console.log(data);
        })
        .catch(e=>{
            console.log(e);
        });
    };
    return (
    <div>
      <br />
      <form  className="d-grid gap-2" style={{margin:"0rem"}} >

                  
              <div className='d-flex justify-content-around'>
                  <Link to={"/myorders"}>
               <button onClick={saveOrder} className="btn btn-dark">Add Order</button>
            </Link>

          </div>
        </form>

       
           
     </div>
   
  );

}

export default Order;