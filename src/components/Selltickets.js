import React ,{useState} from "react";
import { useDispatch,useSelector } from "react-redux";
import { findTicketById } from "../actions/tickets";
import {Link} from "react-router-dom";
import { useEffect } from "react";

const Selltickets=()=>{

    const tickets=useSelector(state=> state.tickets);
    const dispatch=useDispatch();

    useEffect(() => {
        dispatch(findTicketById());
    },[]);


    return (
        <div>
         
        <Link  className ='d-flex justify-content' to="/create">
        <button  className="create-button">Create Ticket</button>
         </Link ><br></br>

         <div className='card card body'>
            <table  className = "table table-striped">
              
                  <thead>
                      <tr>
                        <th> TITLE</th> 
                          <th> PRICE</th>
                          <th> QUANTITY</th>
                      </tr>
                  </thead>
                  <tbody>
                  {
            tickets &&
            tickets.map((ticket,index)=>(

               <tr  key={index}>
                
                <td>{ticket.title}</td>
               <td>$ {ticket.price}</td>
                <td>{ticket.quantity}</td>
                
                <Link  to={"/update"}>
                 <div className='d-flex justify-content'>
                <button  className="button">Update</button>
                </div>
                </Link>  

                </tr>
            ))}

      
        </tbody>
        </table>
        </div>
        </div>
    );
}

export default Selltickets;