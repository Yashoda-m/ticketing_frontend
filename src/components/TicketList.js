import React ,{useEffect} from "react";
import { useDispatch,useSelector } from "react-redux";
import { retrieveTickets } from "../actions/tickets";
import {Link} from "react-router-dom";

const TicketList=()=>{
    

    const tickets=useSelector(state=>state.tickets);
    const dispatch=useDispatch();

    useEffect(()=>{
        dispatch(retrieveTickets());
    },[]);

   

    return (
        <div >
              <h1 className = "title"> Tickets For Sale</h1>
              
              <table className = "table table-striped"><br></br>
                  <thead>
                      
                          <th> TITLE</th>
                          <th> PRICE</th>
                          <th> QUANTITY</th>
                      
                  </thead>

      <tbody>
            {
            tickets &&
            tickets.map((ticket,index)=>(

               <tr  key={index}>
                
                <td>{ticket.title}</td>
               <td>$ {ticket.price}</td>
                <td>{ticket.quantity}</td>
                <Link  to={"/order"}>
                <button  className="button">Purchase</button>
                </Link>  

                </tr>
            ))}

        </tbody>
       
        </table>
        </div>
       

    )
}

export default TicketList;