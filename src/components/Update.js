import React,{useEffect,useState} from "react";
import { useDispatch } from "react-redux";
import { updateTicket } from "../actions/tickets";
import UserService from "../services/UserService";
import { useHistory } from "react-router-dom";

const Update =(props) => {
    const initialTicketState = {
        id:null,
        price:""

    };

    const [currentTicket,setCurrentTicket] = useState(initialTicketState);
   
    let history=useHistory()

    const dispatch = useDispatch();

    const getTicket = ticketId => {
        UserService.get(ticketId)
        .then(response => {
            setCurrentTicket(response.data);
            console.log(response.data);
        })
        .catch(e=>{
            console.log(e);
        });
    };

    useEffect(() => {
        getTicket(props.match.params.ticketId);
    },[props.match.params.ticketId]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentTicket({ ...currentTicket, [name]: value});
    };

    const updateContent = () => {
        dispatch(updateTicket(currentTicket.ticketId,currentTicket))
        .then(response => {
            console.log(response);
        })
        .catch(e => {
            console.log(e);
        });
        history.push("/selltickets");
    };

    return (
        <div>
            <section>
            {currentTicket ? (
        <div className="card card-body mt-3">
          
          <form>
            <div className="form-group">
              <label htmlFor="price">Price</label>
              <input
                type="text"
                className="form-control"
                id="price"
                name="price"
                value={currentTicket.price}
                onChange={handleInputChange}
              />
            </div>
          </form>
          <button
            type="submit"
            className="button"
            onClick={updateContent}
          >
            Update
          </button>
          
        </div>
      ) : (
        <div>
          <br />
        </div>
      )}
           </section> 
        </div>
    )
}

export default Update;