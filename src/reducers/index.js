import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import tickets from "./tickets"

export default combineReducers({
  auth,
  message,
  tickets,
});
