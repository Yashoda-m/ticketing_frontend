import {
    NEW_PAYMENT,
} from "../actions/types";

const initialState = [];

const paymentReducer=(payments= initialState ,action)=>{
    
    const { type, payload } = action;

    switch (type) {
      case NEW_PAYMENT:
        return [...payments, payload];

    
        default:
            return payments;
  
    }
  };
  
  export default paymentReducer;