import {
    CREATE_TICKET,
    RETRIEVE_TICKETS,
    UPDATE_TICKET,
} from "../actions/types";

const initialState = [];

const ticketReducer=(tickets= initialState ,action)=>{
    
    const { type, payload } = action;

    switch (type) {
      case CREATE_TICKET:
        return [...tickets, payload];
  
      case RETRIEVE_TICKETS:
        return payload;

      case UPDATE_TICKET:
        return tickets.map((ticket) => {
          if (ticket.ticketId === payload.ticketId) {
            return {
              ...ticket,
              ...payload,
            };
          } else {
            return ticket;
          }
        });
        default:
            return tickets;
  
    }
  };
  
  export default ticketReducer;