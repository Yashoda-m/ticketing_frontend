import axios from "axios";
import http from "../http-common";

const getAll=()=>{
    return http.get("/tickets");
}

const get = ticketId => {
    const user = JSON.parse(localStorage.getItem('user'));
    return axios.get(`http://localhost:8082/api/tickets/${ticketId}`,
    {
        headers: ( 
         (user && user.accessToken )?{ Authorization: "Bearer "+user.accessToken}:{}
        )
   });
}

const findById=()=>{
    const user = JSON.parse(localStorage.getItem('user'));
    return axios.get('http://localhost:8082/api/user/'+user.id,
    {
         headers: ( 
          (user && user.accessToken )?{ Authorization: "Bearer "+user.accessToken}:{}
         )
    });

};

const create=data=>{
    const user = JSON.parse(localStorage.getItem('user'));
        //console.log("ticket created");
        console.log(user);
        return axios.post('http://localhost:8082/api/tickets/'+user.id,data)

}

const update =(ticketId,data)=>{
  //  const user = JSON.parse(localStorage.getItem("user"));  

    return axios.put(`http://localhost:8082/api/tickets/${ticketId}`,data) 

};

const createOrder=()=>{
    const user = JSON.parse(localStorage.getItem('user'));
    return axios.post('http://localhost:8082/api/orders/'+user.id,
    {
         headers: ( 
          (user && user.accessToken )?{ Authorization: "Bearer "+user.accessToken}:{}
         )
    });
};

const createPayment=()=>{
    const user = JSON.parse(localStorage.getItem('user'));
    return axios.post('http://localhost:8082/api/payment/'+user.id,
    {
         headers: ( 
          (user && user.accessToken )?{ Authorization: "Bearer "+user.accessToken}:{}
         )
    });
}





const UserService={
    getAll,
    get,
    findById,
    create,
    update,
    createOrder,
    createPayment
};


export default UserService;

